package Encapsulation;

public class Karyawan {
    private final double gajiTetap;
    private int noPegawai;
    private String namaPegawai;
    private int gajiBonus;

    public Karyawan() {
        this.gajiTetap = 100;
    }

    public Karyawan(double gajiTetap) {
        this.gajiTetap = gajiTetap;
    }

    public int getNoPegawai() {
        return noPegawai;
    }

    public void setNoPegawai(int noPegawai) {
        this.noPegawai = noPegawai;
    }

    public String getNamaPegawai() {
        return namaPegawai;
    }

    public void setNamaPegawai(String namaPegawai) {
        this.namaPegawai = namaPegawai;
    }

    public int getGajiBonus() {
        return gajiBonus;
    }

    public void setGajiBonus(int gajiBonus) {
        this.gajiBonus = gajiBonus;
    }

    public double karyawanBaru() {
        return gajiTetap;
    }

    public double karyawanLama() {
        this.gajiBonus = 50;
        return gajiTetap + gajiBonus;
    }
}

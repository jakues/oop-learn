package Encapsulation;

public class Tester {
    public static void main(String[] args) {
        Karyawan objBaru = new Karyawan(200);
        objBaru.setNoPegawai(1);
        objBaru.setNamaPegawai("Agos");

        System.out.println("No. pegawai : " + objBaru.getNoPegawai());
        System.out.println("Nama pegawai: " + objBaru.getNamaPegawai());
        System.out.println("Gaji        : $" + objBaru.karyawanBaru());

        System.out.println();
        objBaru.setNoPegawai(2);
        objBaru.setNamaPegawai("Wito");

        System.out.println("No. pegawai : " + objBaru.getNoPegawai());
        System.out.println("Nama pegawai: " + objBaru.getNamaPegawai());
        System.out.println("Gaji        : $" + objBaru.karyawanLama());
    }
}

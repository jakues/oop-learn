#include <iostream>

using namespace std;

int main() {
    int age = 25;
    float height = 170.5;
    double weight = 65.81234115121;
    bool isAlive = true;
    string name = "Owii";
    char blood = 'A';

    // instead of using long
    int8_t num; // 8 bit int
    int16_t num16; // 16 bit
    int32_t num32; // 32 bit
    int64_t num64; // 64 bit

    cout << "Name : " << name << endl;
    cout << "Height : " << height << endl;
    cout << "Weight : " << weight << endl;
    cout << "Age : " << age << endl;
    cout << "Blood type : " << blood << endl;
    cout << "isAlive : " << isAlive << endl;
    cout << num << num16 << num32 << num64;

    return 0;
}
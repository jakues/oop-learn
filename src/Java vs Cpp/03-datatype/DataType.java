class DataType {
    public static void main(String []args) {
        int age = 25;
        Float height = 170.5F; // ends with F
        double weight = 65.81234115121;
        Boolean isAlive = true;
        String name = "Owii";
        char blood = 'A';

        System.out.println("Name : " + name);
        System.out.println("Height : " + height);
        System.out.println("Weight : " + weight);
        System.out.println("Age : " + age);
        System.out.println("Blood type : " + blood);
        System.out.println("isAlive : " + isAlive);
    }
}
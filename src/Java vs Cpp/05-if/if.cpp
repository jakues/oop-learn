#include <cstdlib>
#include <iostream>

using namespace std;

int main() {
  int score, max = 10;
  bool valid;

  // provide seed value
  srand((unsigned) time(NULL));

  // fill it
  score = rand() % max+1;
  valid = true;

  cout << "Your score : " << score << endl;

  if (score >= 9 && valid) {
    cout << "Good\n";
  } else if (score >= 7 && valid) {
    cout << "Neutral\n";
  } else {
    cout << "Bad\n";
  }

  return 0;
}
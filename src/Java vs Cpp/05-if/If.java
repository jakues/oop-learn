import java.util.Random;

class If {
    public static void main(String []args) {
        int score, max = 10;
        Boolean valid;

        Random rand = new Random();
        max += 1;
        score = rand.nextInt(max);
        valid = true;

        System.out.println("Your score : " + score);

        if (score >= 9 && valid) {
            System.out.println("Good");
        } else if (score >= 7 && valid) {
            System.out.println("Neutral");
        } else {
            System.out.println("Bad");
        }
    }
}
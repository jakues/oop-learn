#include <iostream>

using namespace std;

int main() {
    int matrix[3][3];

    // fill array
    for (int i=0; i < 3; i++) {
        for (int j=0; j < 3; j++) {
            if (j == 1) {
                matrix[i][j] = 0;
            } else {
                matrix[i][j] = i+1;
            }
        }
    }

    // show array
    for (int i=0; i < 3; i++) {
        for (int j=0; j < 3; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
}
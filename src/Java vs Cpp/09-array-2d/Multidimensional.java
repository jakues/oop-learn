class Multidimensional {
    public static void main(String []args) {
        int matrix[][];
        matrix = new int[3][3];

        // fill array
        for (int i=0; i < 3; i++) {
            for (int j=0; j < 3; j++) {
                if (j == 1) {
                    matrix[i][j] = 0;
                } else {
                    matrix[i][j] = i+1;
                }
            }
        }

        // show array
        for (int i=0; i < 3; i++) {
            for (int j=0; j < 3; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            if (i != 2) {
                System.out.print("\n");
            }
        }
    }
}
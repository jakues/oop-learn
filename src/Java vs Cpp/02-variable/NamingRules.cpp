#include <iostream>

using namespace std;

int main() {
  // varaible naming rules

  // good
  int myAge = 20;                // camel case
  int YourAge = 21;              // pascal case
  int CURRENT_AGE = YourAge + 1; // snake case
  int 速度 = 1;                  // unicode
  int _savage = 5;               // not recomended

  cout << "Good variable\n";
  cout << "myAge : " << myAge << endl;
  cout << "YourAge: " << YourAge << endl;
  cout << "CURRENT_AGE : " << CURRENT_AGE << endl;
  cout << "\nGood but not recomended\n";
  cout << "Unicode var : " << 速度 << endl;
  cout << "_savage : " << _savage << endl;

  // bad
  int n = 1;                               // avoid single letter
  string psvm = "public static void main"; // avoid acronyms
  cout << "\nBad variable\n";
  cout << "n : " << n << endl;
  cout << "psvm : " << psvm << endl;

  return 0;
}
class MultipleDeclaration {
    public static void main(String []args) {
        System.out.println("Multiple declaration variable\n");

        int num, age;
        num = 5;
        age = num * 5; // vars to vars

        System.out.println(num + " " + age + "\n");

        System.out.println("Another multiple declaration\n");

        int number = 10, year = 2023;

        System.out.printf("%d %d\n", number, year);
    }
}
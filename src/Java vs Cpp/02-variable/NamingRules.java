public class NamingRules {
    public static void main(String []args) {
        // varaible naming rules

        // good
        int myAge = 20; // camel case
        int YourAge = 21; // pascal case
        int CURRENT_AGE = YourAge + 1; // snake case
        int 速度 = 1; // unicode
        int _savage = 5; // not recomended

        System.out.println("Good variable");
        System.out.println("myAge : " + myAge);
        System.out.println("YourAge : " + YourAge);
        System.out.println("CURRENT_AGE : " + CURRENT_AGE);
        System.out.println("\nGood but not recomended");
        System.out.println("Unicode var: " + 速度);
        System.out.println("_savage : " + _savage);

        // bad
        int n = 1; // avoid single letter
        String psvm = "public static void main"; // avoid acronyms

        System.out.println("\nBad variable");
        System.out.println("n : " + n);
        System.out.println("psvm : " + psvm);
    }
}
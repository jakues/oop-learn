#include <iostream>

using namespace std;

int main() {
    int age = 20;
    cout << "age : " << age << endl;

    cout << "re-declare it" << endl;

    age = 25;
    cout << "age : " << age << endl;

    return 0;
}
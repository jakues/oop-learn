#include <iostream>

using namespace std;

int main() {
    cout << "Multiple declaration var\n";

    int num, age;
    num = 5;
    age = 25;

    cout << num << " " << age << endl;

    cout << "Another multiple declaration var\n";

    int number = 2, year = 2023;

    cout << number << " " << year << endl;

    return 0;
}
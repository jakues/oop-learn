#include <iostream>

using namespace std;

class Input {
private:
    string name;
    int age;
public:
  Input(string name, int age) {
    this->name = name;
    this->age = age;
  }

  void userInput() {
    cout << "Input name: ";
    cin >> name;
    cout << "Input age: ";
    cin >> age;
  }

  void output() {
    cout << "Name : " << name << endl;
    cout << "Age : " << age << endl;
  }
};

int main() {
  // input using constructor
  Input person("Arnheid", 20);
  // output it
  person.output();

  cout << endl;
  // input using public func
  // we replace it
  person.userInput();
  // output it
  person.output();
}
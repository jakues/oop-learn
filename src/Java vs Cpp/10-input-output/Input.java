import java.util.Scanner;

class Input {
    private String name;
    private int age;
    Input(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void userInput() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input name : ");
        name = input.nextLine();

        System.out.println("Input age : ");
        age = input.nextInt();
    }

    public void output() {
        System.out.println("Name : " + name);
        System.out.println("Age : " + age);
    }

    public static void main(String []args) {
        // input using constructor
        Input person = new Input("Arnheid", 20);
        person.output();

        // input using scanner
        person.userInput();
        person.output();
    }
}
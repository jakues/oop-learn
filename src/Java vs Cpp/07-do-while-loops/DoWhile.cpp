#include <iostream>

using namespace std;

int main() {
  int max = 3;
  int i = 1;
  do {
    int j = 1;
    do {
      cout << i << " " << j << endl;
      j++;
    } while (j <= max);
    i++;
  } while (i <= max);

  return 0;
}
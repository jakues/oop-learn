class Array {
    public static void main(String []args) {
        // declare array integer
        int[] age;

        // allocation memory : 5 int
        age = new int[5];

        // fill array
        for (int i=0; i < 5; i++) {
            age[i] = (i+1) * 10;
        }

        // show array
        for (int i=0; i < 5; i++) {
            System.out.print(age[i] + " ");
        }
    }
}
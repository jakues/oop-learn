#include <iostream>

using namespace std;

int main() {
    int age[5];

    // fill age
    for (int i=0; i < 5; i++) {
        age[i] = (i+1) * 10;
    }

    // show array
    for (int i=0; i < 5; i++) {
        cout << age[i] << " ";
    }

    return 0;
}
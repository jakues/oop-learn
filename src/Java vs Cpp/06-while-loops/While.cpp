#include <cstdlib>
#include <iostream>

using namespace std;

int main() {
  int min, max = 10, sum = 0;

  // generate random lower bound
  srand((unsigned)time(NULL));
  min = rand() % max + 1;

  while (min < max) {
    if ((!(min % 2)) != 0) {
      sum += min;
      cout << min;
      if (min < max - 2) {
        cout << " + ";
      }
    }
    min++;
  }

  cout << " = " << sum << endl;

  return 0;
}
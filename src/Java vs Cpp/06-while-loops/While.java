import java.util.Random;

class While {
    public static void main(String []args) {
        int min, max = 10, sum = 0;

        Random rand = new Random();
        max += 1;
        min = rand.nextInt(max);

        // sum only even
        while (min < max) {
            if (min % 2 == 0) {
                sum += min;
                System.out.print(min);
                if (min < max - 2) {
                    System.out.print(" + ");
                }
            }
            min++;
        }

        System.out.print(" = " + sum);
    }
}
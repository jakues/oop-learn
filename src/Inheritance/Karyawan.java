package Inheritance;

public class Karyawan {
    private String NIK;
    private String nama;
    private double gajiTetap = 500;

    Karyawan(String NIK, String nama) {
        this.NIK = NIK;
        this.nama = nama;
    }

    public String getNIK() {
        return NIK;
    }

    public String getNama() {
        return nama;
    }

    public double getGajiTetap() {
        return gajiTetap;
    }
}

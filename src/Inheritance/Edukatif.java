package Inheritance;

public class Edukatif extends Karyawan {
    private String NIDN;
    private double honorMengajar;
    private double tunjangan;

    public Edukatif(String NIK, String nama, String NIDN) {
        this(NIK, nama, NIDN, 100, 200);
    }

    public Edukatif(String NIK, String nama, String NIDN, double honorMengajar, double tunjangan) {
        super(NIK, nama);
        this.NIDN = NIDN;
        this.honorMengajar = honorMengajar;
        this.tunjangan = tunjangan;
    }

    public String getNIDN() {
        return NIDN;
    }

    public double getHonorMengajar() {
        return honorMengajar;
    }

    public double getTunjangan() {
        return tunjangan;
    }
}

package Inheritance;

public class NonEdukatif extends Karyawan {
    private double honorKehadiran;
    private double uangLembur;

    public NonEdukatif(String NIK, String nama) {
        this(NIK, nama, 50, 100);
    }

    public NonEdukatif(String NIK, String nama, double honorKehadiran, double uangLembur) {
        super(NIK, nama);
        this.honorKehadiran = honorKehadiran;
        this.uangLembur = uangLembur;
    }
    public double getHonorKehadiran() {
        return honorKehadiran;
    }

    public double getUangLembur() {
        return uangLembur;
    }
}

package Inheritance;

public class Main {
    public static void main(String []args) {
        Edukatif obj1 = new Edukatif("123", "Agos", "001");
        double gajiObj1 = obj1.getGajiTetap() + obj1.getHonorMengajar() + obj1.getTunjangan();
        System.out.println(gajiObj1);

        NonEdukatif obj2 = new NonEdukatif("333", "Yolo", 1000, 1000);
        double gajiObj2 = obj2.getGajiTetap() + obj2.getHonorKehadiran() + obj2.getUangLembur();
        System.out.println(gajiObj2);

        NonEdukatif obj3 = new NonEdukatif("9999", "Manto");
        double gajiObj3 = obj3.getGajiTetap() + obj3.getHonorKehadiran();
        System.out.println(gajiObj3);

        Edukatif obj4 = new Edukatif("13218", "Wito", "004");
        double gajiObj4 = obj4.getGajiTetap() + obj4.getHonorMengajar();
        System.out.println(gajiObj4);
    }
}
